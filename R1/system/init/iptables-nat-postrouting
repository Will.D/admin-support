#! /bin/bash

## Init script for iptables nat POSTROUTING chain

tag=firewall
ipt=/usr/sbin/iptables
log=/usr/bin/logger

table=nat
chain=POSTROUTING
rules_script=/system/sbin/iptables/iptables-nat-postrouting

if [ ! -f "$rules_script" ]; then
	$log --id=$$ --tag=$tag "Error: $rules_script is missing"
	exit 1
fi

if [ ! -x "$rules_script" ]; then
	$log --id=$$ --tag=$tag "Error: $rules_script is not executable"
	exit 1
fi


start() {
	$log --id=$$ --tag=$tag "Starting $table $chain ruleset script"
	$rules_script
	return $?
}


stop() {
	$log --id=$$ --tag=$tag "Stopping $table $chain ruleset script"
	$ipt -t $table -F $chain
	$ipt -t $table -P $chain ACCEPT
	return $?
}

restart() {
	stop && start
	return $?
}

usage() {
	echo "Usage: $(basename $0) <start|stop|restart>" >&2
	return 2
}


case "$1" in
	start   ) start   ;;
	stop    ) stop    ;;
	restart ) restart ;;
	*       ) usage   ;;
esac

exit $?
